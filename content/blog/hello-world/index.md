---
title: Hello World
date: "2020-01-20"
description: "Hello World"
---

This is the first post on the new Front End Nerdery blog!

As I get more time freed up after finishing up some projects, I'll be concentrating on this project more.

Here's a great video from Gatsby with an introduction to Gatsby.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/WFOxZAmB_1Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
